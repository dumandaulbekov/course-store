const { Router } = require('express');
const Course = require('../models/course');
const router = Router();

router.get('/', async (req, res) => {
    const courses = await Course.find().lean();

    res.render('index', {
        title: 'news',
        isNews: true,
        courses
    });
});

module.exports = router;


