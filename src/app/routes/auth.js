const { Router } = require('express');
const router = Router();
const bcrypt = require('bcryptjs');
const User = require('../models/user');

router.get('/login', async (req, res) => {
    res.render('auth/login', {
        title: 'Authorization',
        isLogin: true,
        loginError: req.flash('loginError'),
        registerError: req.flash('registerError')
    });
});

router.get('/logout', async (req, res) => {
    req.session.destroy(() => {
        res.redirect('/auth/login');
    });
});

router.post('/login', async (req, res) => {
    try {
        const { email, password } = req.body;
        const condidate = await User.findOne({ email });

        if (condidate) {
            const areSame = await bcrypt.compare(password, condidate.password);
            if (areSame) {
                const user = condidate;
                req.session.user = user;
                req.session.isAuthenticated = true;

                req.session.save(err => {
                    if (err) {
                        throw err;
                    }
                    res.redirect('/');
                });
            } else {
                req.flash('loginError', 'Incorrect password')
                res.redirect('/auth/login#login');
            }
        } else {
            req.flash('loginError', 'Not found user')
            res.redirect('/auth/login#login');
        }

    } catch (error) {
        console.log(error);
    }
});

router.post('/register', async (req, res) => {
    try {
        const { email, password, repeat, name } = req.body
        const condidate = await User.findOne({ email });

        if (condidate) {
            req.flash('registerError', 'A user with this email already exists.');
            res.redirect('/auth/login#register');
        } else {
            const hashPassword = await bcrypt.hash(password, 10);

            const user = new User({
                email, name, password: hashPassword, cart: { items: [] }
            });

            await user.save();
            res.redirect('/auth/login#login');
        }

    } catch (error) {
        console.log('register', error);
    }

});

module.exports = router;