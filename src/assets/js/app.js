// Courses
const toCurrency = price => {
    return new Intl.NumberFormat('en-US', {
        currency: 'usd',
        style: 'currency'
    }).format(price);
}

document.querySelectorAll('#price').forEach(node => {
    node.textContent = toCurrency(node.textContent);
});

document.querySelectorAll('#randomNum').forEach(node => {
    node.textContent = getRandomNum(0, 100);
})

document.querySelectorAll('#date').forEach(node => {
    node.textContent = getRandomNum(1, 365) + ' days ago';
});

document.querySelectorAll('#description').forEach(node => {
    switch (getRandomNum(1, 3)) {
        case 1:
            node.textContent = `The only course you need to learn web development - HTML, CSS, JS, Node, and More!`
            break;
        case 2:
            node.textContent = `Master Digital Marketing: Strategy, Social Media Marketing, SEO, YouTube, Email, Facebook Marketing, Analytics & More!`
            break;
        default:
            node.textContent = `Become an ethical hacker that can hack computer systems like black hat hackers and secure them like security experts.`
            break;
    }
});

document.querySelectorAll('#name').forEach(node => {
    switch (getRandomNum(1, 10)) {
        case 1:
            node.textContent = 'Dias Zhaksylykov';
            break;
        case 2:
            node.textContent = 'Nurzhan Mukhametkali';
            break;
        case 3:
            node.textContent = 'Al-farabi Rakhimzhanov';
            break;
        case 4:
            node.textContent = 'Duman Daulbekov';
            break;
        case 5:
            node.textContent = 'Vladilen Minin';
            break;
        case 6:
            node.textContent = 'Ryan Dahl';
            break;
        case 7:
            node.textContent = 'Brendan Eich';
            break;
        case 8:
            node.textContent = `Matt D'Avella`;
            break;
        case 9:
            node.textContent = 'Thomas Frank';
            break;
        default:
            node.textContent = 'Secret Person';
            break;
    }
});

function getRandomNum(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

// Card 
const $card = document.querySelector('#basket')

if ($card) {
    $card.addEventListener('click', event => {
        if (event.target.classList.contains('js-remove')) {
            const id = event.target.dataset.id;
            const csrf = event.target.dataset.csrf;

            fetch('/card/remove/' + id, {
                method: 'delete',
                headers: {
                    'X-XSRF-TOKEN': csrf
                }
            }).then(res => res.json())
                .then(card => {
                    if (card.courses.length) {
                        const html = card.courses.map(c => {
                            return `
                            <tr>
                                <th scope="row">${c.title}</th>
                                <td id="price">${c.price}</td>
                                <td>${c.count}</td>
                                <td>
                                    <button class="btn btn-danger btn-sm js-remove"
                                    data-csrf="${csrf}" data-id="${c._id}">Remove</button>
                                </td>
                            </tr>
                            `
                        }).join('');
                        $card.querySelector('tbody').innerHTML = html;
                        $card.querySelector('.totalPrice').textContent = toCurrency(card.price);
                    } else {
                        $card.innerHTML = '<p>Empty</p>'
                    }
                });
        }
    });
};

// date 
const toDate = date => {
    return new Intl.DateTimeFormat('ru-Ru', {
        day: '2-digit',
        month: 'long',
        year: 'numeric'
    }).format(new Date(date));
};

document.querySelectorAll('#orderDate').forEach(node => {
    node.textContent = toDate(node.textContent);
});
