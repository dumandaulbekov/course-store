const express = require('express');
const path = require('path');
const csrf = require('csurf');
const flash = require('connect-flash');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongodb-session')(session);

const homeRoutes = require('./src/app/routes/news');
const addRoutes = require('./src/app/routes/add');
const coursesRoutes = require('./src/app/routes/courses');
const newsRoutes = require('./src/app/routes/news');
const cardRoutes = require('./src/app/routes/card');
const ordersRoutes = require('./src/app/routes/orders');
const authRoutes = require('./src/app/routes/auth');
const varMiddleware = require('./src/app/middleware/variables');
const userMiddleware = require('./src/app/middleware/user');

const MONGODB_URL = 'mongodb+srv://duman:pfdJshQoeM0lu5F9@cluster0-b83qj.mongodb.net/course-store'

const app = express();

const store = new MongoStore({
    collection: 'sessions',
    uri: MONGODB_URL
});

app.engine('hbs', exphbs({
    defaultLayout: 'main',
    extname: 'hbs'
}));

app.set('view engine', 'hbs');
app.set('views', 'src/app/views');

app.use(express.static(path.join(__dirname, 'src/assets')));
app.use(express.urlencoded({ extended: true }));

// settings session
app.use(session({
    secret: 'secret value',
    resave: false,
    saveUninitialized: false,
    store
}));
app.use(csrf());
app.use(flash());
app.use(varMiddleware);
app.use(userMiddleware);

app.use('/', homeRoutes);
app.use('/add', addRoutes);
app.use('/courses', coursesRoutes);
app.use('/index', newsRoutes);
app.use('/card', cardRoutes);
app.use('/orders', ordersRoutes);
app.use('/auth', authRoutes);
// Run port
const PORT = process.env.PORT || 3000;

async function start() {
    try {
        await mongoose.connect(MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });

    } catch (error) {
        console.log('index', error);
    }
}

start();


